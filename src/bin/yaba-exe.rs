use clap::Parser;
use fuser::MountOption;
use hakoniwa::{ExecutorResult, ExecutorResultStatus, Sandbox, Stdio};
use prost::Message;
use reapi::build::bazel::remote::execution::v2::command::EnvironmentVariable;
use reapi::build::bazel::remote::execution::v2::{
    Action, ActionResult, Command, Digest, OutputDirectory,
};
use std::fs;
use std::io::{stdin, stdout, Read, Write};
use std::path::{Path, PathBuf};
use std::sync::mpsc::channel;
use tempdir::TempDir;
use yaba::fuse::ReapiFilesystem;
use yaba::{
    digest_from_tree, directory_blob_to_tree_block, directory_from_digest_block, read_blob_block,
    upload_blob_block,
};

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
    #[clap(long)]
    remote: Option<String>,
    #[clap(long)]
    action: Option<String>,
    #[clap(long)]
    action_result: Option<String>,
    #[clap(long)]
    no_logs_capture: bool,
    #[clap(long)]
    cas_root: Option<PathBuf>,
    #[clap(long)]
    interactive_digest: Option<String>,
}

fn execute_action(
    arguments: Vec<String>,
    network: bool,
    mountpoint: &Path,
    environment_variables: &Vec<EnvironmentVariable>,
    interactive: bool,
) -> hakoniwa::ExecutorResult {
    let sandbox = Sandbox::new();
    let prog = arguments.first().unwrap();
    let argv = arguments.clone();
    let mut executor = sandbox.command(prog, &argv);
    for env in environment_variables {
        executor.setenv(&env.name, &env.value);
    }
    executor.rw_bind("/dev/null", "/dev/null").unwrap();
    executor.rw_bind("/dev/random", "/dev/random").unwrap();
    executor.rw_bind("/dev/urandom", "/dev/urandom").unwrap();
    executor.rw_bind("/dev/zero", "/dev/zero").unwrap();
    //executor.rw_bind("tmpfs", "/tmp/").unwrap();
    executor
        ._bind("", "/tmp/", Some("tmpfs"), Some(true))
        .unwrap();
    executor.uid(0);
    executor.gid(0);

    if interactive {
        executor.stdout(Stdio::inherit());
        executor.stderr(Stdio::inherit());
        executor.stdin(Stdio::inherit());
    }
    //executor
    //   .rw_bind("/etc/resolv.conf", "/etc/resolv.conf")
    //  .unwrap();
    //executor
    //   .rw_bind("/etc/ssl/certs", "/etc/ssl/certs/")
    //  .unwrap();
    if network {
        executor.share_net_ns(true);
    }
    //executor.share_uts_ns(true);

    println!("mountpoint {mountpoint:?}");
    executor.container_root_dir(mountpoint).unwrap();

    println!("Execute {executor:?}");
    let result = executor.run();

    println!(
        "Standard out and error of container {:?} {:?}",
        String::from_utf8_lossy(&result.stdout),
        String::from_utf8_lossy(&result.stderr)
    );

    result
}

fn main() {
    let cli = Cli::parse();
    //tracing_subscriber::fmt().init();
    //env_logger::init();

    let root = if let Some(root) = cli.cas_root {
        root
    } else {
        PathBuf::from("/home/will/.cache/girderstream/cas/")
    };
    let build_dir = root.join("tmp/");
    fs::create_dir_all(&build_dir).unwrap();
    let mountpoint_owner = TempDir::new_in(build_dir, "hi").unwrap();
    let mountpoint = mountpoint_owner.path();
    let options = vec![
        MountOption::RW,
        MountOption::FSName("hello".to_string()),
        //MountOption::AutoUnmount,
    ];

    let (root_digest, command, interactive) = if let Some(di_string) = cli.interactive_digest {
        let command = Command {
            arguments: vec![
                "/usr/bin/sh".to_string(),
                "-ce".to_string(),
                "true".to_string(),
            ],
            environment_variables: vec![],
            output_files: vec![],
            output_directories: vec![],
            output_paths: vec![],
            platform: None,
            working_directory: "/".to_string(),
            output_node_properties: vec![],
        };
        let mut parts = di_string.split('/');
        let root_digest = Digest {
            hash: parts.next().unwrap().to_string(),
            size_bytes: parts.next().unwrap().parse::<i64>().unwrap(),
        };
        (root_digest, command, true)
    } else {
        let mut action_file = std::fs::File::open(cli.action.unwrap()).unwrap();
        let mut action_raw = vec![];
        action_file.read_to_end(&mut action_raw).unwrap();
        let action = Action::decode(action_raw.as_slice()).unwrap();
        let command = action.command_digest.unwrap();
        let command = Command::decode(read_blob_block(&root, &command).as_slice()).unwrap();
        let root_digest = action.input_root_digest.unwrap();
        let interactive = cli.no_logs_capture;
        (root_digest, command, interactive)
    };
    let (sender, receiver) = channel();

    let cas_fs = ReapiFilesystem::new(root.to_owned(), root_digest, sender);
    println!("cas_fs: {cas_fs:?}");

    let filesystem = fuser::spawn_mount2(cas_fs, mountpoint, &options).unwrap();

    println!("command: {:?}", command.arguments);

    let arguments = command.arguments.clone();

    let mut network = false;
    println!("Check for network");
    if let Some(plat) = command.platform {
        for prop in plat.properties {
            if prop.name == "network" && prop.value == "on" {
                network = true;
                println!("Setting network on")
            }
        }
    }

    let result: Option<ExecutorResult> = if true {
        let result = execute_action(
            arguments,
            network,
            mountpoint,
            &command.environment_variables,
            interactive,
        );
        //println!("Results: {result:?}");
        Some(result)
    } else {
        println!("{:?}", mountpoint);
        let mut s = String::new();
        print!("Please enter some text: ");
        let _ = stdout().flush();
        stdin()
            .read_line(&mut s)
            .expect("Did not enter a correct string");
        println!("You typed: {}", s);
        None
    };

    println!("Un mounting file system");

    drop(filesystem);
    println!("Un mounted");

    let root_digest = receiver.recv().unwrap();
    println!("reply: {root_digest:?}");

    drop(mountpoint_owner);
    let mut output_directories = vec![];
    for path in command.output_paths {
        if path == "." {
            output_directories.push(OutputDirectory {
                path: path.clone(),
                tree_digest: Some(digest_from_tree(
                    &root,
                    &directory_blob_to_tree_block(&root, root_digest.clone()),
                )),
            })
        } else {
            let root_dir = directory_from_digest_block(&root, root_digest.clone());
            for subdir in &root_dir.directories {
                if subdir.name == path {
                    output_directories.push(OutputDirectory {
                        path: subdir.name.clone(),
                        tree_digest: Some(digest_from_tree(
                            &root,
                            &directory_blob_to_tree_block(&root, subdir.digest.clone().unwrap()),
                        )),
                    })
                }
            }
        }
    }
    if let Some(result) = result {
        let stdout_blob = upload_blob_block(&root, result.stdout);
        let stderr_blob = upload_blob_block(&root, result.stderr);

        let reapi_result = ActionResult {
            output_files: vec![],
            output_file_symlinks: vec![],
            output_symlinks: vec![],
            output_directories,
            output_directory_symlinks: vec![],
            exit_code: result.exit_code.unwrap_or(99),
            stdout_raw: vec![],
            stdout_digest: Some(stdout_blob),
            stderr_raw: vec![],
            stderr_digest: Some(stderr_blob),
            execution_metadata: None,
        };

        println!("out {:?}", &cli.action_result);
        let mut output = fs::File::create(cli.action_result.unwrap()).unwrap();
        let mut buf = vec![];
        reapi_result.encode(&mut buf).unwrap();
        output.write_all(&buf).unwrap();
        println!("out {:?}", buf);

        assert_eq!(result.status, ExecutorResultStatus::Ok);
        assert_eq!(result.exit_code, Some(0));
    }
}

#[cfg(test)]
mod tests {
    use std::{fs::create_dir_all, process::Command, sync::mpsc::channel};

    use fuser::MountOption;
    use reapi::build::bazel::remote::execution::v2::{
        command::EnvironmentVariable, Action, Digest, Directory,
    };
    use tempfile::{tempdir, TempDir};
    use yaba::{digest_from_directory, fuse::ReapiFilesystem};

    use crate::execute_action;
    #[test]
    fn basic_sandbox() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let extract = Command::new("tar")
            .arg("-xf")
            .arg("busybox.tar.xz")
            .arg("-C")
            .arg(format!("{}", tmp_root.display()))
            .output()
            .unwrap();
        assert_eq!(extract.status.code().unwrap(), 0);
        let busy_box_root = tmp_root.join("busybox2");

        let tree = Command::new("tree").arg(&busy_box_root).output().unwrap();
        println!("tree: {tree:?}");
        println!("{}", String::from_utf8_lossy(&tree.stdout));
        assert_eq!(tree.status.code().unwrap(), 0);

        let ls_bin = busy_box_root.join("bin/ls");
        println!("ls bin {:?}", ls_bin);
        let ls = Command::new(ls_bin)
            .arg("-l")
            .arg(&busy_box_root)
            .output()
            .unwrap();
        println!("ls: {ls:?}");
        println!("{}", String::from_utf8_lossy(&ls.stdout));
        assert_eq!(ls.status.code().unwrap(), 0);

        let args = vec!["/bin/ls".to_string(), "-l".to_string(), "/".to_string()];
        let mountpoint = busy_box_root.clone();
        let environment_variables = vec![EnvironmentVariable {
            name: "PATH".to_string(),
            value: "/bin:/usr/bin".to_string(),
        }];
        let result = execute_action(args, false, &mountpoint, &environment_variables, false);

        println!("result: {result:?}");
        assert_eq!(result.exit_code.unwrap(), 0);

        drop(tmp_dir_test)
    }

    #[test]
    fn cat_cat() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let extract = Command::new("tar")
            .arg("-xf")
            .arg("busybox.tar.xz")
            .arg("-C")
            .arg(format!("{}", tmp_root.display()))
            .output()
            .unwrap();
        assert_eq!(extract.status.code().unwrap(), 0);
        let busy_box_root = tmp_root.join("busybox2");

        let args = vec![
            "/bin/sh".to_string(),
            "-c".to_string(),
            "-e".to_string(),
            "mkdir test\ncd test\ncat > bob.txt << EOF\nshort\nEOF\ncat > bob.txt << EOF\nlong long long\n long long\n long\nEOF\ncat > bob.txt <<EOF\nshort\nEOF\ncat bob.txt\n".to_string(),
        ];
        let mountpoint = busy_box_root.clone();
        let environment_variables = vec![EnvironmentVariable {
            name: "PATH".to_string(),
            value: "/bin:/usr/bin".to_string(),
        }];
        let result = execute_action(args, false, &mountpoint, &environment_variables, false);

        println!("result: {result:?}");
        assert_eq!(result.exit_code.unwrap(), 0);
        let out = String::from_utf8(result.stdout).unwrap();
        assert!(out.contains("short"));
        assert!(!out.contains("long"));

        drop(tmp_dir_test)
    }

    #[test]
    fn ln() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let extract = Command::new("tar")
            .arg("-xf")
            .arg("busybox.tar.xz")
            .arg("-C")
            .arg(format!("{}", tmp_root.display()))
            .output()
            .unwrap();
        assert_eq!(extract.status.code().unwrap(), 0);
        let busy_box_root = tmp_root.join("busybox2");

        let args = vec![
            "/bin/sh".to_string(),
            "-c".to_string(),
            "-e".to_string(),
            "mkdir test\ncd test\ncat > bob.txt << EOF\nshort\nEOF\nln bob.txt cat.txt\ncat cat.txt\n".to_string(),
        ];
        let mountpoint = busy_box_root.clone();
        let environment_variables = vec![EnvironmentVariable {
            name: "PATH".to_string(),
            value: "/bin:/usr/bin".to_string(),
        }];
        let result = execute_action(args, false, &mountpoint, &environment_variables, false);

        println!("result: {result:?}");
        assert_eq!(result.exit_code.unwrap(), 0);
        let out = String::from_utf8(result.stdout).unwrap();
        assert!(out.contains("short"));
        assert!(!out.contains("long"));

        drop(tmp_dir_test)
    }

    #[test]
    fn second_sandbox() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let extract = Command::new("tar")
            .arg("-xf")
            .arg("busybox.tar.xz")
            .arg("-C")
            .arg(format!("{}", tmp_root.display()))
            .output()
            .unwrap();
        assert_eq!(extract.status.code().unwrap(), 0);
        let busy_box_root = tmp_root.join("busybox2");

        let tree = Command::new("tree").arg(&busy_box_root).output().unwrap();
        println!("tree: {tree:?}");
        println!("{}", String::from_utf8_lossy(&tree.stdout));
        assert_eq!(tree.status.code().unwrap(), 0);

        let ls_bin = busy_box_root.join("bin/ls");
        println!("ls bin {:?}", ls_bin);
        let ls = Command::new(ls_bin)
            .arg("-l")
            .arg(&busy_box_root)
            .output()
            .unwrap();
        println!("ls: {ls:?}");
        println!("{}", String::from_utf8_lossy(&ls.stdout));
        assert_eq!(ls.status.code().unwrap(), 0);

        let args = vec![
            "/bin/sh".to_string(),
            "-c".to_string(),
            "-e".to_string(),
            "ls -l; true".to_string(),
        ];
        let mountpoint = busy_box_root.clone();
        let environment_variables = vec![EnvironmentVariable {
            name: "PATH".to_string(),
            value: "/bin:/usr/bin".to_string(),
        }];
        let result = execute_action(args, false, &mountpoint, &environment_variables, false);

        println!("result: {result:?}");
        assert_eq!(result.exit_code.unwrap(), 0);

        let args = vec![
            "/bin/sh".to_string(),
            "-c".to_string(),
            "-e".to_string(),
            "ls -l; false".to_string(),
        ];
        let mountpoint = busy_box_root.clone();
        let environment_variables = vec![EnvironmentVariable {
            name: "PATH".to_string(),
            value: "/bin:/usr/bin".to_string(),
        }];
        let result = execute_action(args, false, &mountpoint, &environment_variables, false);

        println!("result: {result:?}");
        assert_eq!(result.exit_code.unwrap(), 1);
        drop(tmp_dir_test)
    }

    fn sub_pro() {
        let action = Action {
            command_digest: Some(Digest {
                hash: "".to_string(),
                size_bytes: 0,
            }),
            input_root_digest: todo!(),
            timeout: todo!(),
            do_not_cache: todo!(),
            salt: todo!(),
            platform: todo!(),
        };
    }
}
