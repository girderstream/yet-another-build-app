use clap::{crate_version, Arg, ArgAction, Command};
use fuser::MountOption;
use reapi::build::bazel::remote::execution::v2::Digest;
use std::io::{stdin, stdout, Write};
use std::path::PathBuf;
use std::sync::mpsc::channel;
use yaba::fuse::ReapiFilesystem;

fn main() {
    let matches = Command::new("hello")
        .version(crate_version!())
        .author("Christopher Berner")
        .arg(
            Arg::new("MOUNT_POINT")
                .required(true)
                .index(1)
                .help("Act as a client, and mount FUSE at given path"),
        )
        .arg(
            Arg::new("auto_unmount")
                .long("auto_unmount")
                .action(ArgAction::SetTrue)
                .help("Automatically unmount on process exit"),
        )
        .arg(
            Arg::new("allow-root")
                .long("allow-root")
                .action(ArgAction::SetTrue)
                .help("Allow root user to access filesystem"),
        )
        .get_matches();
    //env_logger::init();
    tracing_subscriber::fmt().init();
    let mountpoint = matches.get_one::<String>("MOUNT_POINT").unwrap();
    let mut options = vec![MountOption::RW, MountOption::FSName("hello".to_string())];
    if matches.get_flag("auto_unmount") {
        options.push(MountOption::AutoUnmount);
    }
    if matches.get_flag("allow-root") {
        options.push(MountOption::AllowRoot);
    }

    let (sender, receiver) = channel();

    let cas_fs = ReapiFilesystem::new(
        PathBuf::from("/home/will/.cache/girderstream/cas"),
        Digest {
            hash: "215282d240f0edde8cfe72f5d1a0908b874db227433a185e6fbb04b6d980f516".to_string(),
            size_bytes: 1183,
        },
        sender,
    );

    println!("cas_fs: {cas_fs:?}");

    let filesystem = fuser::spawn_mount2(cas_fs, mountpoint, &options).unwrap();

    let mut s = String::new();
    print!("Please enter some text: ");
    let _ = stdout().flush();
    stdin()
        .read_line(&mut s)
        .expect("Did not enter a correct string");
    println!("You typed: {}", s);

    drop(filesystem);

    let reply = receiver.recv().unwrap();
    println!("reply: {reply:?}");
}
