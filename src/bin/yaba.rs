use base64::prelude::*;
use clap::Parser;
use http::HeaderMap;
use prost_types::Any;
use sha2::{Digest as _, Sha256};
use std::fs::{create_dir_all, FileTimes};
use std::time::SystemTime;
use std::{
    cmp::min,
    fs,
    io::SeekFrom,
    net::SocketAddr,
    path::{Path, PathBuf},
    sync::Arc,
};
use tempfile::tempdir;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncSeekExt;
use tokio::{fs::remove_file, fs::File, io::AsyncWriteExt, sync::mpsc};
use tokio_stream::wrappers::ReceiverStream;
use tonic::metadata::MetadataMap;
use tonic::{
    async_trait, codegen::http, transport::Server, Code, Request, Response, Status, Streaming,
};
use tracing::{info, span, Level, Span};

use prost::Message;
use reapi::{
    build::bazel::{
        remote::{
            asset::v1::{
                fetch_server::{Fetch, FetchServer},
                push_server::{Push, PushServer},
                FetchBlobRequest, FetchBlobResponse, FetchDirectoryRequest, FetchDirectoryResponse,
                PushBlobRequest, PushBlobResponse, PushDirectoryRequest, PushDirectoryResponse,
                Qualifier,
            },
            execution::v2::{
                capabilities_server::{Capabilities, CapabilitiesServer},
                content_addressable_storage_server::{
                    ContentAddressableStorage, ContentAddressableStorageServer,
                },
                digest_function,
                execution_server::{Execution, ExecutionServer},
                symlink_absolute_path_strategy, Action, ActionCacheUpdateCapabilities,
                BatchReadBlobsRequest, BatchReadBlobsResponse, BatchUpdateBlobsRequest,
                BatchUpdateBlobsResponse, CacheCapabilities, Digest, ExecuteRequest,
                FindMissingBlobsRequest, FindMissingBlobsResponse, GetCapabilitiesRequest,
                GetTreeRequest, GetTreeResponse, RequestMetadata, ServerCapabilities, ToolDetails,
                WaitExecutionRequest,
            },
        },
        semver::SemVer,
    },
    google::{
        bytestream::{
            byte_stream_server::{ByteStream, ByteStreamServer},
            QueryWriteStatusRequest, QueryWriteStatusResponse, ReadRequest, ReadResponse,
            WriteRequest, WriteResponse,
        },
        longrunning::Operation,
    },
};

use yaba::{digest_to_path, digest_to_tmp_path, read_blob};

use lazy_static::lazy_static;
use regex::Regex;

const MAX_BATCH_BYTES: i64 = 4 * 1024 * 1024;
const BLOCK_SIZE: usize = 1024 * 1024;

fn get_metadata<T>(req: &Request<T>) -> Option<Arc<RequestMetadata>> {
    req.extensions().get::<Arc<RequestMetadata>>().cloned()
}

trait HeaderGet {
    fn get(&self, key: &str) -> Option<&[u8]>;
}

impl HeaderGet for HeaderMap {
    fn get(&self, key: &str) -> Option<&[u8]> {
        self.get(key).map(|v| v.as_bytes())
    }
}

impl HeaderGet for MetadataMap {
    fn get(&self, key: &str) -> Option<&[u8]> {
        self.get_bin(key).map(|v| v.as_ref())
    }
}

fn extract_metadata(headers: &impl HeaderGet) -> Option<RequestMetadata> {
    headers
        .get("build.bazel.remote.execution.v2.requestmetadata-bin")
        .and_then(|v| BASE64_STANDARD.decode(v).ok())
        .and_then(|v| RequestMetadata::decode(v.as_ref()).ok())
}

fn tracing_span(req: &http::Request<()>) -> Span {
    if let Some(metadata) = extract_metadata(req.headers()) {
        let metadata: RequestMetadata = metadata;
        let tool = metadata
            .tool_details
            .as_ref()
            .map(|tool_details| format!("{}/{}", tool_details.tool_name, tool_details.tool_version))
            .unwrap_or_else(|| "unknown".into());
        let invocation = metadata.tool_invocation_id;
        span!(Level::INFO, "", tool = ?tool, invocation = ?invocation)
    } else {
        Span::none()
    }
}
fn _show_metadata(mut req: Request<()>) -> Result<Request<()>, Status> {
    if let Some(metadata) = extract_metadata(req.metadata()) {
        req.extensions_mut().insert(Arc::new(metadata));
    } else {
        let metadata = RequestMetadata {
            tool_details: Some(ToolDetails {
                tool_name: "unknown".into(),
                tool_version: "0.0.0".into(),
            }),
            action_id: "".into(),
            tool_invocation_id: "unknown".into(),
            correlated_invocations_id: "unknown".into(),
            action_mnemonic: "unknown".into(),
            target_id: "unknown".into(),
            configuration_id: "unknown".into(),
        };
        req.extensions_mut().insert(Arc::new(metadata));
    }
    Ok(req)
}

struct YabaCapabilities {
    _location: PathBuf,
}

impl YabaCapabilities {
    fn new(location: PathBuf) -> Self {
        Self {
            _location: location,
        }
    }
}

#[async_trait]
impl Capabilities for YabaCapabilities {
    async fn get_capabilities(
        &self,
        request: Request<GetCapabilitiesRequest>,
    ) -> Result<Response<ServerCapabilities>, Status> {
        if let Some(metadata) = get_metadata(&request) {
            info!(
                "Handling capabilities request for invocation {}",
                metadata.tool_invocation_id
            );
        };
        info!("Handling capabilities");
        if request.get_ref().instance_name == *"" {
            Ok(Response::new(ServerCapabilities {
                cache_capabilities: Some(CacheCapabilities {
                    digest_functions: vec![digest_function::Value::Sha256 as i32],
                    action_cache_update_capabilities: Some(ActionCacheUpdateCapabilities {
                        update_enabled: true,
                    }),
                    cache_priority_capabilities: None,
                    max_batch_total_size_bytes: MAX_BATCH_BYTES,
                    symlink_absolute_path_strategy:
                        symlink_absolute_path_strategy::Value::Disallowed as i32,
                    supported_compressors: vec![],
                    supported_batch_update_compressors: vec![],
                }),
                execution_capabilities: None,
                deprecated_api_version: None,
                low_api_version: Some(SemVer {
                    major: 2,
                    minor: 0,
                    patch: 0,
                    prerelease: "".into(),
                }),
                high_api_version: Some(SemVer {
                    major: 2,
                    minor: 2,
                    patch: 0,
                    prerelease: "".into(),
                }),
            }))
        } else {
            Err(Status::new(
                Code::InvalidArgument,
                format!("Unknown instance '{}'", request.get_ref().instance_name),
            ))
        }
    }
}

struct YabaCasStore {
    location: PathBuf,
}

impl YabaCasStore {
    fn new(location: PathBuf) -> Self {
        Self { location }
    }
}

#[async_trait]
impl ContentAddressableStorage for YabaCasStore {
    type GetTreeStream = ReceiverStream<Result<GetTreeResponse, Status>>;

    async fn find_missing_blobs(
        &self,
        request: Request<FindMissingBlobsRequest>,
    ) -> Result<Response<FindMissingBlobsResponse>, Status> {
        info!("Find Missing blobs");
        let missing_blob_request = request.into_inner();
        let mut response = FindMissingBlobsResponse {
            missing_blob_digests: Vec::with_capacity(missing_blob_request.blob_digests.len()),
        };

        for blob in missing_blob_request.blob_digests {
            if blob == *EMPTY_DIGEST {
                continue;
            }
            let new_path = digest_to_path(&self.location, &blob);

            if !new_path.exists() {
                response.missing_blob_digests.push(blob);
            } else {
                match std::fs::File::options().write(true).open(new_path) {
                    Ok(fh) => {
                        let times = FileTimes::new().set_accessed(SystemTime::now());
                        if let Ok(ac) = fh.set_times(times) {
                            info!("updated time {ac:?}")
                        } else {
                            info!("did not update time")
                        };
                    }
                    Err(er) => {
                        info!("could not open to update err {er:?}")
                    }
                };
                //let times = FileTimes::new().set_accessed(SystemTime::now());
                //fh.set_times(times)?;
            }
        }
        Ok(Response::new(response))
    }

    async fn batch_update_blobs(
        &self,
        _request: Request<BatchUpdateBlobsRequest>,
    ) -> Result<Response<BatchUpdateBlobsResponse>, Status> {
        todo!()
    }

    async fn batch_read_blobs(
        &self,
        _request: Request<BatchReadBlobsRequest>,
    ) -> Result<Response<BatchReadBlobsResponse>, Status> {
        todo!()
    }

    async fn get_tree(
        &self,
        _request: Request<GetTreeRequest>,
    ) -> Result<Response<Self::GetTreeStream>, Status> {
        todo!()
    }
}

struct YabaByteStream {
    location: PathBuf,
}

impl YabaByteStream {
    fn new(location: PathBuf) -> Self {
        Self { location }
    }
}

lazy_static! {
    static ref WRITE_RESOURCE_PARTS: Regex =
        Regex::new("^(.*?)/?uploads/[^/]+?/blobs/([a-f0-9]{64})/([0-9]+)").unwrap();
    static ref READ_RESOURCE_PARTS: Regex =
        Regex::new("^(.*?)/?blobs/([a-f0-9]{64})/([0-9]+)").unwrap();
    static ref EMPTY_DIGEST: Digest = Digest {
        hash: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855".to_string(),
        size_bytes: 0,
    };
}

fn read_blob_digest(raw_string: &str) -> Digest {
    let parts = READ_RESOURCE_PARTS.captures(raw_string).unwrap();

    // println!("parts {parts:?}");
    // let instance_name = parts.get(1).map(|m| m.as_str()).unwrap_or("");

    let hash = parts.get(2).unwrap().as_str();
    let size = parts.get(3).unwrap().as_str();
    let size = size.parse().unwrap();

    Digest {
        hash: hash.to_string(),
        size_bytes: size,
    }
}

fn write_blob_digest(raw_string: &str) -> Digest {
    let parts = WRITE_RESOURCE_PARTS.captures(raw_string).unwrap();

    // println!("parts {parts:?}");
    // let instance_name = parts.get(1).map(|m| m.as_str()).unwrap_or("");

    let hash = parts.get(2).unwrap().as_str();
    let size = parts.get(3).unwrap().as_str();
    let size = size.parse().unwrap();

    Digest {
        hash: hash.to_string(),
        size_bytes: size,
    }
}

#[async_trait]
impl ByteStream for YabaByteStream {
    type ReadStream = ReceiverStream<Result<ReadResponse, Status>>;

    async fn query_write_status(
        &self,
        _request: Request<QueryWriteStatusRequest>,
    ) -> Result<Response<QueryWriteStatusResponse>, Status> {
        Err(Status::new(Code::NotFound, "We never have partial writes"))
    }

    async fn write(
        &self,
        request: Request<Streaming<WriteRequest>>,
    ) -> Result<Response<WriteResponse>, Status> {
        info!("write: {request:?}");
        let mut wstream = request.into_inner();
        let firstmsg = match wstream.message().await? {
            Some(msg) => msg,
            None => return Err(Status::new(Code::InvalidArgument, "No write messages?")),
        };

        let digest = write_blob_digest(&firstmsg.resource_name);
        if digest == *EMPTY_DIGEST {
            let response = WriteResponse {
                committed_size: digest.size_bytes,
            };
            while let Some(mes) = wstream.message().await.unwrap() {
                if mes.finish_write {
                    break;
                }
            }
            //             println!("finished good");
            return Ok(Response::new(response));
        }
        //println!("write di {digest:?}");
        let new_path = digest_to_path(&self.location, &digest);
        info!("new_path {new_path:?}");

        if new_path.exists() {
            info!("Already got it {new_path:?}");
            let response = WriteResponse {
                committed_size: digest.size_bytes,
            };
            if !firstmsg.finish_write {
                while let Some(mes) = wstream.message().await.unwrap() {
                    if mes.finish_write {
                        break;
                    }
                }
            }
            //    println!("finished good");
            return Ok(Response::new(response));
        }

        let parent = new_path.parent().unwrap();
        info!("parent: {parent:?}");

        create_dir_all(parent)?;

        let tmp_file_name = digest_to_tmp_path(&self.location, &digest);
        let parent = tmp_file_name.parent().unwrap();
        create_dir_all(parent)?;

        let mut tmp_file = File::create(&tmp_file_name).await?;
        let mut dig = Sha256::new();
        let mut upload_size = 0;

        dig.update(&firstmsg.data);
        upload_size += firstmsg.data.len();
        tmp_file.write_all(&firstmsg.data).await?;

        let mut finish_write = firstmsg.finish_write;

        while let Some(msg) = wstream.message().await? {
            // Check the write_offset
            tmp_file.write_all(&msg.data).await?;
            dig.update(&msg.data);
            upload_size += msg.data.len();
            // Make sure the last chunk is a finish_write
            finish_write = msg.finish_write;
        }

        if !finish_write {
            return Err(Status::new(
                Code::Cancelled,
                "You stopped uploading, you fool!",
            ));
        }

        let uploaded_digest = Digest {
            hash: hex::encode(dig.finalize()).to_string(),
            size_bytes: upload_size as i64,
        };

        if uploaded_digest != digest {
            tmp_file.shutdown().await.unwrap();
            remove_file(&tmp_file_name).await.unwrap();

            return Err(Status::new(
                Code::InvalidArgument,
                format!(
                    "Uploaded data has digest {}/{} whereas upload said it would be {}/{}",
                    uploaded_digest.hash,
                    uploaded_digest.size_bytes,
                    digest.hash,
                    digest.size_bytes
                ),
            ));
        }

        if atomicwrites::move_atomic(&tmp_file_name, &new_path).is_err() {
            return Err(Status::new(
                Code::Internal,
                format!("Could not move blob into place {}", uploaded_digest.hash),
            ));
        };

        let response = WriteResponse {
            committed_size: digest.size_bytes,
        };

        println!("wrote it");
        Ok(Response::new(response))
    }

    async fn read(
        &self,
        request: Request<ReadRequest>,
    ) -> Result<Response<Self::ReadStream>, Status> {
        info!("read");
        let di = read_blob_digest(&request.get_ref().resource_name);
        info!("read di {di:?}");

        if di == *EMPTY_DIGEST {
            let (tx, rx) = mpsc::channel(4);
            let data: Vec<u8> = vec![];
            let msg = ReadResponse { data };
            tx.send(Ok(msg)).await.unwrap();
            return Ok(Response::new(ReceiverStream::new(rx)));
        }

        let new_path = digest_to_path(&self.location, &di);
        //println!("read: new_path {new_path:?}");

        if !new_path.exists() {
            return Err(Status::new(Code::NotFound, "Not found"));
        }

        let read_start = request.get_ref().read_offset as usize;
        let read_total = request.get_ref().read_limit as usize;

        let mut blob_file = File::open(&new_path).await.unwrap();
        blob_file
            .seek(SeekFrom::Start(read_start as u64))
            .await
            .unwrap();

        let blob_len = blob_file.metadata().await.unwrap().len() as usize;

        if blob_len < read_start {
            return Err(Status::new(Code::InvalidArgument, "offset out of range"));
        }

        let read_total = if read_total == 0 {
            blob_len
        } else {
            read_total
        };

        let read_total = min((blob_len) - read_start, read_total) as usize;

        let (tx, rx) = mpsc::channel(4);
        tokio::spawn(async move {
            let mut sent: usize = 0;
            while sent < read_total {
                let mut buffer = vec![0; BLOCK_SIZE];
                let read_size = match blob_file.read(&mut buffer).await {
                    Ok(size) => size,
                    Err(_e) => {
                        panic!("bad reading");
                        //tx.send(Err(e)).await.unwrap();
                        //break;
                    }
                };
                buffer.resize(read_size, 0);
                tx.send(Ok(ReadResponse { data: buffer })).await.unwrap();
                sent += read_size;
            }
            //println!("finished {sent} {read_total}");
        });

        Ok(Response::new(ReceiverStream::new(rx)))
    }
}

fn asset_digest_to_path(root: &Path, digest: &str) -> PathBuf {
    let root = root.join("assets/objects");
    root.join(&digest[..2]).join(&digest[2..])
}

struct YabaPullAssets {
    location: PathBuf,
}

impl YabaPullAssets {
    fn new(location: PathBuf) -> Self {
        Self { location }
    }
}

fn request_to_sha_bb(uri: String, qualifiers: Vec<Qualifier>) -> String {
    let ref_req = FetchBlobRequest {
        instance_name: "".to_string(),
        timeout: None,
        oldest_content_accepted: None,
        uris: vec![uri],
        qualifiers,
    };

    let any_req = Any {
        type_url: "type.googleapis.com/build.bazel.remote.asset.v1.FetchBlobRequest".to_string(),
        value: ref_req.encode_to_vec(),
    };

    //    println!(
    //        "encode {}",
    //        String::from_utf8_lossy(&any_req.encode_to_vec())
    //    );

    let mut dig = Sha256::new();
    dig.update(any_req.encode_to_vec());

    //   println!("hex_dd {hex_d}");
    hex::encode(dig.finalize()).to_string()
}

fn _request_to_sha(uri: &str) -> String {
    let mut dig = Sha256::new();
    dig.update("blob:v1:");
    dig.update(uri);

    // println!("hex_new {hex_b}");
    hex::encode(dig.finalize()).to_string()
}

#[async_trait]
impl Push for YabaPullAssets {
    async fn push_blob(
        &self,
        req: Request<PushBlobRequest>,
    ) -> Result<Response<PushBlobResponse>, Status> {
        let push_request = req.into_inner();

        let asset_key = request_to_sha_bb(push_request.uris.first().unwrap().clone(), vec![]);

        let fbr = FetchBlobResponse {
            status: None,
            uri: push_request.uris.first().unwrap().to_string(),
            qualifiers: vec![],
            expires_at: None,
            blob_digest: push_request.blob_digest,
        };

        let any_req = Any {
            // this is done to match casd, it confuses me greatly why this is called a fetch but is a push
            type_url: "type.googleapis.com/build.bazel.remote.asset.v1.FetchBlobResponse"
                .to_string(),

            // i think this should be fetch to be complient with bb
            value: fbr.encode_to_vec(),
        };

        let mut encoded_result = vec![];
        any_req.encode(&mut encoded_result).unwrap();

        let new_path = asset_digest_to_path(&self.location, &asset_key);
        print!("new_path: {new_path:?}");
        create_dir_all(new_path.parent().unwrap()).unwrap();
        let mut asset_file = File::create(&new_path).await?;
        asset_file.write_all(&encoded_result).await?;

        Ok(Response::new(PushBlobResponse {}))
    }

    async fn push_directory(
        &self,
        _req: Request<PushDirectoryRequest>,
    ) -> Result<Response<PushDirectoryResponse>, Status> {
        Err(Status::unimplemented("not doing directories yet.."))
    }
}

struct YabaFetchAssets {
    location: PathBuf,
}

impl YabaFetchAssets {
    fn new(location: PathBuf) -> Self {
        Self { location }
    }
}

#[async_trait]
impl Fetch for YabaFetchAssets {
    async fn fetch_blob(
        &self,
        req: Request<FetchBlobRequest>,
    ) -> Result<Response<FetchBlobResponse>, Status> {
        //println!("req: {req:?}");
        let fetch_request = req.into_inner();

        let asset_key_bb = request_to_sha_bb(fetch_request.uris.first().unwrap().clone(), vec![]);
        //  println!("asset_key_bb {asset_key_bb:?}");
        let path_name = asset_digest_to_path(&self.location, &asset_key_bb);
        //println!("path_name {path_name:?}");

        if !path_name.exists() {
            return Err(Status::new(Code::NotFound, "blob not found".to_string()));
        }

        let mut asset_file_bb = File::open(path_name).await.unwrap();
        let mut raw_buff_bb = vec![];
        asset_file_bb.read_to_end(&mut raw_buff_bb).await?;

        // always encode to disk as fetch if push or pull
        let from_disk = match Any::decode(raw_buff_bb.as_slice()) {
            Ok(from_disk) => {
                //      println!("from disk {from_disk:?}");

                let from_disk = FetchBlobResponse::decode(from_disk.value.as_slice()).unwrap();
                from_disk
            }
            Err(_e) => {
                //      println!("from disk err {e:?}");
                return Err(Status::new(Code::NotFound, "Artifact not found"));
            }
        };

        Ok(Response::new(from_disk))
    }

    async fn fetch_directory(
        &self,
        _req: Request<FetchDirectoryRequest>,
    ) -> Result<Response<FetchDirectoryResponse>, Status> {
        Err(Status::not_found(
            "unable to find any matching directory asset",
        ))
    }
}

struct YabaExecution {
    location: PathBuf,
}

impl YabaExecution {
    fn new(location: PathBuf) -> Self {
        Self { location }
    }
}

#[async_trait]
impl Execution for YabaExecution {
    type ExecuteStream = ReceiverStream<Result<Operation, Status>>;

    async fn execute(
        &self,
        request: Request<ExecuteRequest>,
    ) -> Result<Response<Self::ExecuteStream>, Status> {
        let (_m, _e, er) = request.into_parts();

        let a = er.action_digest.unwrap();
        let action_raw = read_blob(&self.location, &a).await;
        //println!("action: {:?}",  String::from_utf8_lossy(&action_raw));
        let action = Action::decode(action_raw.as_slice()).unwrap();
        println!("action: {action:?}");

        let tmp_dir_test = tempdir()?;
        let _tmp_dir_test_path = tmp_dir_test.path();

        todo!()
    }

    type WaitExecutionStream = ReceiverStream<Result<Operation, Status>>;

    async fn wait_execution(
        &self,
        _request: Request<WaitExecutionRequest>,
    ) -> Result<Response<Self::WaitExecutionStream>, Status> {
        todo!()
    }
}

async fn serve(dst: SocketAddr, location: Option<PathBuf>) {
    let location = if let Some(location) = location {
        location
    } else {
        PathBuf::from(std::env::var("HOME").unwrap()).join(".cache/girderstream/cas/")
    };
    println!("location: {:?}", location);

    if !location.exists() {
        fs::create_dir_all(&location).unwrap();
    }
    let location = location.canonicalize().unwrap();
    println!("location: {:?}", location);

    let caps_server = YabaCapabilities::new(location.clone());
    let cas_server = YabaCasStore::new(location.clone());
    let byte_stream_server = YabaByteStream::new(location.clone());
    let asset_pull_server = YabaPullAssets::new(location.clone());
    let asset_fetch_server = YabaFetchAssets::new(location.clone());
    let execution_server = YabaExecution::new(location);

    Server::builder()
        .trace_fn(tracing_span)
        .add_service(CapabilitiesServer::new(caps_server))
        .add_service(ContentAddressableStorageServer::new(cas_server))
        .add_service(ByteStreamServer::new(byte_stream_server))
        .add_service(PushServer::new(asset_pull_server))
        .add_service(FetchServer::new(asset_fetch_server))
        .add_service(ExecutionServer::new(execution_server))
        .serve(dst)
        .await
        .unwrap();
}

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
pub struct Cli {
    /// Specify the url of the local cas endpoint
    #[clap(long)]
    bind: Option<String>,
    /// Specify the instance of the local cas
    location: Option<PathBuf>,
    #[arg(short, long)]
    protect_session_blobs: bool,
}

#[tokio::main]
async fn main() {
    println!("Hello, world!");
    tracing_subscriber::fmt().init();
    println!("Serve, world!");
    let cli = Cli::parse();
    println!("cli: {cli:?}");

    println!("{:?}", cli.bind);
    let port = if let Some(dir) = cli.bind {
        dir.split(':').last().unwrap().parse::<u16>().unwrap()
    } else {
        50040
    };

    serve(([0, 0, 0, 0], port).into(), cli.location).await;
}
