use crate::{
    digest_from_directory, digest_to_path, directory_from_digest_block, read_blob_block,
    upload_blob_block,
};
use core::panic;
use fuser::{
    FileAttr, FileType, Filesystem, ReplyAttr, ReplyCreate, ReplyData, ReplyDirectory,
    ReplyDirectoryPlus, ReplyEmpty, ReplyEntry, ReplyOpen, ReplyWrite, Request, TimeOrNow,
};
use libc::{EACCES, EIO, ENOENT, ENOSYS, O_NOATIME};
use nix::unistd::{Gid, Uid};
use std::cmp::min;
use std::ffi::OsStr;
use std::io::{Read, Seek, SeekFrom, Write};
use std::os::unix::ffi::OsStrExt;
use std::path::{Path, PathBuf};
use std::sync::mpsc::Sender;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use tempfile::{tempdir_in, TempDir};
use tracing::{debug, error, info};

use reapi::build::bazel::remote::execution::v2::{
    Digest, Directory, DirectoryNode, FileNode, SymlinkNode,
};

const TTL: Duration = Duration::from_secs(1); // 1 second

// Emulate a device with 4kB logical blocks matching common storage devices.
//#define LOGICAL_BLOCKSIZE 4096
const LOGICAL_BLOCKSIZE: u32 = 4096;

const HELLO_DIR_ATTR: FileAttr = FileAttr {
    ino: 1,
    size: 0,
    blocks: 0,
    atime: UNIX_EPOCH, // 1970-01-01 00:00:00
    mtime: UNIX_EPOCH,
    ctime: UNIX_EPOCH,
    crtime: UNIX_EPOCH,
    kind: FileType::Directory,
    perm: 0o755,
    nlink: 2,
    uid: 1000,
    gid: 1000,
    rdev: 0,
    flags: 0,
    blksize: LOGICAL_BLOCKSIZE,
};

const HELLO_FILE_ATTR: FileAttr = FileAttr {
    ino: 0,
    size: 0,
    blocks: 0,
    atime: UNIX_EPOCH, // 1970-01-01 00:00:00
    mtime: UNIX_EPOCH,
    ctime: UNIX_EPOCH,
    crtime: UNIX_EPOCH,
    kind: FileType::RegularFile,
    perm: 0o644,
    nlink: 1,
    uid: 1000,
    gid: 1000,
    rdev: 0,
    flags: 0,
    blksize: LOGICAL_BLOCKSIZE,
};

const NONE_ATTR: FileAttr = FileAttr {
    ino: 0,
    size: 0,
    blocks: 0,
    atime: UNIX_EPOCH, // 1970-01-01 00:00:00
    mtime: UNIX_EPOCH,
    ctime: UNIX_EPOCH,
    crtime: UNIX_EPOCH,
    kind: FileType::RegularFile,
    perm: 0,
    nlink: 0,
    uid: 0,
    gid: 0,
    rdev: 0,
    flags: 0,
    blksize: 0,
};

const HELLO_LINK_ATTR: FileAttr = FileAttr {
    ino: 3,
    size: 13,
    blocks: 1,
    atime: UNIX_EPOCH, // 1970-01-01 00:00:00
    mtime: UNIX_EPOCH,
    ctime: UNIX_EPOCH,
    crtime: UNIX_EPOCH,
    kind: FileType::Symlink,
    perm: 0o777,
    nlink: 1,
    uid: 1000,
    gid: 1000,
    rdev: 0,
    flags: 0,
    blksize: LOGICAL_BLOCKSIZE,
};

enum ActiveDirectoryNode {
    File(usize, ActiveDirectoryFile),
    Dir(usize, (u64, String)),
    Link(usize, (u64, String)),
}

#[derive(Debug, Clone)]
struct ActiveInode(u64);

#[derive(Debug)]
struct ActiveFile {
    tempdir: TempDir,
    attra: FileAttr,
    root: PathBuf,
}

impl ActiveFile {
    fn from(root_dir: &Path, blob: Digest, root: PathBuf) -> Self {
        let raw = read_blob_block(root_dir, &blob);
        let mut new = ActiveFile {
            tempdir: tempdir_in(root).unwrap(),
            attra: HELLO_FILE_ATTR,
            root: root_dir.to_owned(),
        };
        new.attra.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        new.attra.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        new.attra.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        let mut file_handle = std::fs::File::create(new.file_name()).unwrap();
        file_handle.write_all(&raw).unwrap();
        file_handle.flush().unwrap();
        new
    }
    fn new(root_dir: &Path, root: PathBuf) -> Self {
        let mut attra = HELLO_FILE_ATTR;
        attra.mtime = SystemTime::now();
        attra.atime = SystemTime::now();
        attra.ctime = SystemTime::now();
        attra.crtime = SystemTime::now();
        let new = ActiveFile {
            tempdir: tempdir_in(root).unwrap(),
            attra,
            root: root_dir.to_owned(),
        };
        std::fs::File::create(new.file_name()).unwrap();
        new
    }
    fn file_name(&self) -> PathBuf {
        self.tempdir.path().join("data")
    }
    fn get_digest(&mut self) -> Digest {
        //self.tempfile.seek(SeekFrom::Start(0)).unwrap();
        let mut file = std::fs::File::open(self.file_name()).unwrap();
        let mut blob = vec![];
        file.flush().unwrap();
        file.read_to_end(&mut blob).unwrap();

        upload_blob_block(&self.root, blob)
    }
}

#[derive(Debug)]
struct ActiveDirectoryFile {
    inode: ActiveInode,
    file_name: String,
    _root: PathBuf,
}

#[derive(Debug)]
struct ActiveDirectory {
    parent: u64,
    // the inode that this file is located at and it name
    files: Vec<ActiveDirectoryFile>,
    // the inode that this directory is located at and it name
    directories: Vec<(u64, String)>,
    // the inode that this link is located at and it name
    links: Vec<(u64, String)>,
    root: PathBuf,
}

impl ActiveDirectory {
    fn new(root_dir: PathBuf, parent: u64) -> Self {
        Self {
            parent,
            files: vec![],
            directories: vec![],
            links: vec![],
            root: root_dir,
        }
    }
    fn from(
        root_dir: PathBuf,
        all_inodes: &mut Vec<INode>,
        inode: u64,
        parent: u64,
        digest: Digest,
    ) -> Self {
        let directory = directory_from_digest_block(&root_dir, digest);
        //println!("ActiveINode from {directory:?}");
        let mut files = Vec::with_capacity(directory.files.len());
        for file in directory.files {
            let di = file.digest.unwrap().clone();
            let mut attr = HELLO_FILE_ATTR;
            attr.uid = Uid::current().as_raw();
            attr.gid = Gid::current().as_raw();
            attr.size = di.size_bytes as u64;
            attr.blocks = attr.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
            attr.perm += 0o111 * file.is_executable as u16;
            attr.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
            attr.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
            attr.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
            let ino = all_inodes.len() as u64;
            attr.ino = ino;
            all_inodes.push(INode::FileBlob(CasFile {
                _parent: inode,
                blob: di,
                attr,
            }));
            files.push(ActiveDirectoryFile {
                inode: ActiveInode(ino),
                file_name: file.name.clone(),
                _root: root_dir.to_owned(),
            });
        }

        let mut directories = Vec::with_capacity(directory.directories.len());
        for child_directory in directory.directories {
            all_inodes.push(INode::DirBlob((
                inode,
                child_directory.digest.unwrap().clone(),
            )));
            directories.push((all_inodes.len() as u64 - 1, child_directory.name.clone()))
        }

        let mut links = Vec::with_capacity(directory.symlinks.len());
        for link in directory.symlinks {
            let mut new_link = ActiveLink::new(inode, link.target.clone());
            let ino = all_inodes.len() as u64;
            new_link.attra.ino = ino;
            all_inodes.push(INode::Link(new_link));
            links.push((ino, link.name.clone()))
        }

        ActiveDirectory {
            parent,
            files,
            directories,
            links,
            root: root_dir,
        }
    }

    fn get_digest(&self, all_inodes: &mut Vec<INode>) -> Digest {
        let mut directory = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };
        for (symlink_ino, symlink_name) in &self.links {
            let node = all_inodes.get(symlink_ino.to_owned() as usize).unwrap();
            let target = if let INode::Link(link) = node {
                link.target.clone()
            } else {
                panic!("link inode not a link")
            };
            let node = SymlinkNode {
                name: symlink_name.to_owned(),
                target,
                node_properties: None,
            };
            directory.symlinks.push(node);
        }

        for (dir_ino, dir_name) in &self.directories {
            let dummy = INode::Link(ActiveLink::new(0, "".to_string()));
            all_inodes.push(dummy);

            //let root = self.i_nodes.swap_remove(1);
            let node = all_inodes.swap_remove(dir_ino.to_owned() as usize);
            let digest = if let INode::DirBlob((_, blob)) = node {
                blob.to_owned()
            } else if let INode::ActiveDirectory(active_dir) = node {
                active_dir.get_digest(all_inodes)
            } else {
                panic!("link inode not a link")
            };
            let node = DirectoryNode {
                name: dir_name.to_owned(),
                digest: Some(digest),
            };
            directory.directories.push(node);
        }

        for active_file in &self.files {
            let node = all_inodes.get_mut(active_file.inode.0 as usize).unwrap();
            let (digest, attr) = if let INode::FileBlob(cas_file) = node {
                (cas_file.blob.to_owned(), cas_file.attr)
            } else if let INode::ActiveFile(active_file) = node {
                (active_file.get_digest(), active_file.attra)
            } else {
                panic!("link inode not a link")
            };
            // TODO: go through attr and add extra node_properties
            let node = FileNode {
                name: active_file.file_name.to_owned(),
                digest: Some(digest),
                is_executable: (attr.perm & 0o100) == 0o100,
                node_properties: None,
            };
            directory.files.push(node);
        }

        digest_from_directory(&self.root, directory)
    }
}

#[derive(Debug)]
struct ActiveLink {
    _parent: u64,
    target: String,
    attra: FileAttr,
}

impl ActiveLink {
    fn new(parent: u64, target: String) -> Self {
        let mut attra = HELLO_LINK_ATTR;
        attra.size = target.as_bytes().len() as u64;
        attra.uid = Uid::current().as_raw();
        attra.gid = Gid::current().as_raw();
        attra.blocks = attra.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
        attra.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        attra.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        attra.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        Self {
            _parent: parent,
            target,
            attra,
        }
    }
}

#[derive(Debug, Clone)]
struct CasFile {
    _parent: u64,
    blob: Digest,
    attr: FileAttr,
}

#[derive(Debug)]
enum INode {
    // parent, Digest
    FileBlob(CasFile),
    ActiveFile(ActiveFile),
    DirBlob((u64, Digest)),
    ActiveDirectory(ActiveDirectory),
    // parent, target
    Link(ActiveLink),
}

#[derive(Debug)]
pub struct ReapiFilesystem {
    reply: Sender<Digest>,
    i_nodes: Vec<INode>,
    root: PathBuf,
}

impl ReapiFilesystem {
    pub fn new(root: PathBuf, root_digest: Digest, replier: Sender<Digest>) -> Self {
        ReapiFilesystem {
            reply: replier,
            i_nodes: vec![
                INode::FileBlob(CasFile {
                    _parent: 0,
                    blob: Digest {
                        hash: "".to_string(),
                        size_bytes: 0,
                    },
                    attr: NONE_ATTR,
                }),
                INode::DirBlob((1, root_digest)),
            ],
            root,
        }
    }

    fn ensure_active(&mut self, ino: u64) {
        let new: Option<INode> = if let Some(inode) = self.i_nodes.get(ino as usize) {
            if let INode::DirBlob(inode_blob) = inode {
                Some(INode::DirBlob(inode_blob.clone()))
            } else if let INode::FileBlob(inode_blob) = inode {
                Some(INode::FileBlob(inode_blob.clone()))
            } else {
                None
            }
        } else {
            None
        };

        let new: Option<INode> = if let Some(blob) = new {
            if let INode::DirBlob((parent, digest)) = blob {
                let active = ActiveDirectory::from(
                    self.root.to_owned(),
                    &mut self.i_nodes,
                    ino,
                    parent.to_owned(),
                    digest.clone(),
                );

                Some(INode::ActiveDirectory(active))
            } else if let INode::FileBlob(cas_blob) = blob {
                let mut active = ActiveFile::from(&self.root, cas_blob.blob, self.root.clone());
                active.attra.ino = ino;
                active.attra.perm = cas_blob.attr.perm;
                Some(INode::ActiveFile(active))
            } else {
                None
            }
        } else {
            None
        };

        if let Some(inode) = self.i_nodes.get_mut(ino as usize) {
            if let Some(new) = new {
                *inode = new;
                //println!("i_nodes after {} {:?}", ino, self.i_nodes);
            }
        };
    }
}

fn newbit(ino: u64, active: &ActiveDirectory) -> Vec<(u64, i64, FileType, String)> {
    let mut reply: Vec<(u64, i64, FileType, String)> = vec![];
    //println!("newbit {} {:?}", ino, active);

    reply.push((ino, 1_i64, FileType::Directory, ".".to_string()));
    reply.push((active.parent, 2_i64, FileType::Directory, "..".to_string()));

    let mut offset = 3;
    for (dir_inode, dir_name) in &active.directories {
        reply.push((
            *dir_inode,
            offset as i64,
            FileType::Directory,
            dir_name.to_string(),
        ));
        offset += 1;
    }
    for active_file in &active.files {
        reply.push((
            active_file.inode.0,
            offset as i64,
            FileType::RegularFile,
            active_file.file_name.clone(),
        ));
        offset += 1;
    }

    for (link_inode, link_name) in &active.links {
        reply.push((
            *link_inode,
            offset as i64,
            FileType::Symlink,
            link_name.to_string(),
        ));
        offset += 1;
    }

    reply
}

impl Filesystem for ReapiFilesystem {
    /// Look up a directory entry by name and get its attributes.
    fn lookup(&mut self, _req: &Request<'_>, parent: u64, name: &OsStr, reply: ReplyEntry) {
        info!("look up {name:?}");
        if let Some(_dir) = self.i_nodes.get(parent as usize) {
        } else {
            reply.error(EIO);
            return;
        };
        self.ensure_active(parent);
        if let Some(dir) = self.i_nodes.get(parent as usize) {
            match dir {
                INode::DirBlob((_inode, _digest)) => {
                    println!("Should never be able to get here");
                    reply.error(EIO);
                }
                INode::ActiveDirectory(active) => {
                    //if parent == 1 && name.to_str() == Some("hello.txt") {

                    for (dir_inode, dir_name) in &active.directories {
                        if dir_name.as_str() == name.to_str().unwrap() {
                            let mut new_attr = HELLO_DIR_ATTR;
                            new_attr.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                            new_attr.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                            new_attr.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                            new_attr.ino = *dir_inode;
                            new_attr.uid = Uid::current().as_raw();
                            new_attr.gid = Gid::current().as_raw();
                            let inner_dir = self.i_nodes.get(*dir_inode as usize).unwrap();
                            new_attr.nlink = 2 + if let INode::ActiveDirectory(dir) = inner_dir {
                                dir.directories.len()
                            } else if let INode::DirBlob(dir) = inner_dir {
                                let dir = directory_from_digest_block(&self.root, dir.1.clone());
                                dir.directories.len()
                            } else {
                                panic!("bob")
                            } as u32;
                            //println!("na: {name:?} {:#o} {new_attr:?}", new_attr.perm);
                            reply.entry(&TTL, &new_attr, 0);
                            return;
                        }
                    }

                    for active_file in &active.files {
                        if active_file.file_name.as_str() == name.to_str().unwrap() {
                            let file = self.i_nodes.get(active_file.inode.0 as usize).unwrap();
                            match file {
                                INode::ActiveFile(file) => {
                                    let mut new_attr = file.attra;
                                    new_attr.size = file.file_name().metadata().unwrap().len();
                                    new_attr.blocks =
                                        new_attr.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
                                    //println!("look up {file:?} {new_attr:?}");
                                    reply.entry(&TTL, &new_attr, 0);
                                    // println!("na: {name:?} {:#o} {new_attr:?}", new_attr.perm);
                                    return;
                                }
                                INode::FileBlob(cas_blob) => {
                                    reply.entry(&TTL, &cas_blob.attr, 0);
                                    return;
                                }
                                INode::DirBlob(_) => {
                                    error!("ino {file:?} is a dir not a file");
                                }
                                INode::ActiveDirectory(_) => {
                                    error!("ino {file:?} is a dir not a file");
                                }
                                INode::Link(_) => {
                                    error!("ino {file:?} is a link not a file");
                                }
                            }

                            reply.error(EIO);
                            return;
                        }
                    }

                    for (link_inode, link_name) in &active.links {
                        if link_name.as_str() == name.to_str().unwrap() {
                            let link = self.i_nodes.get(*link_inode as usize).unwrap();
                            if let INode::Link(link) = link {
                                let new_attr = link.attra;
                                println!("link {:?} {:?} {:?}", link_name, link, dir);
                                assert_eq!(new_attr.ino, *link_inode);
                                reply.entry(&TTL, &new_attr, 0);
                                return;
                            } else {
                                panic!("link issues")
                            }
                        }
                    }
                    //println!("not found");

                    let mut attr = NONE_ATTR;
                    attr.size = 0;
                    attr.ino = 0;
                    attr.blocks = 0;
                    //println!("attr {attr:?}");
                    //reply.entry(&TTL, &attr, 0);
                    reply.error(ENOENT);
                }
                INode::FileBlob(_) => {
                    error!("Lookup on file inode");
                    reply.error(EIO);
                }
                INode::ActiveFile(_file) => {
                    error!("Lookup on file inode");
                    reply.error(EIO);
                }
                INode::Link(_link) => {
                    error!("Lookup on link inode");
                    reply.error(EIO);
                }
            }
        }
    }

    /// Get file attributes.
    //fn getattr(&mut self, _req: &Request<'_>, ino: u64, fh: Option<u64>, reply: ReplyAttr) {
    fn getattr(&mut self, _req: &Request<'_>, ino: u64, reply: ReplyAttr) {
        info!("get attr: {ino:?}");

        if let Some(inode) = self.i_nodes.get(ino as usize) {
            let attr = match inode {
                INode::FileBlob(cas_blob) => cas_blob.attr,
                INode::ActiveFile(file) => {
                    let mut new_attr = file.attra;
                    info!("active: {:?}", new_attr);
                    info!("file: {:?}", file);
                    new_attr.size = file.file_name().metadata().unwrap().len();
                    new_attr.blocks = new_attr.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
                    new_attr.ino = ino;
                    new_attr
                }
                INode::DirBlob(_) => {
                    let mut attr = HELLO_DIR_ATTR;
                    attr.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.uid = Uid::current().as_raw();
                    attr.gid = Gid::current().as_raw();
                    attr.ino = ino;
                    attr.size = 10;
                    attr
                }
                INode::ActiveDirectory(_) => {
                    let mut attr = HELLO_DIR_ATTR;
                    attr.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.uid = Uid::current().as_raw();
                    attr.gid = Gid::current().as_raw();
                    attr.ino = ino;
                    attr.size = 10;
                    attr
                }
                INode::Link(link) => {
                    let attr = link.attra;
                    assert_eq!(attr.ino, ino);
                    attr
                }
            };
            //println!("get attra {:#o} {attr:?}", attr.perm);
            reply.attr(&TTL, &attr)
        } else {
            reply.error(ENOSYS)
        }
    }

    fn create(
        &mut self,
        _req: &Request<'_>,
        parent: u64,
        name: &OsStr,
        mode: u32,
        _umask: u32,
        flags: i32,
        reply: ReplyCreate,
    ) {
        //println!("create {:?} {:?}", name, mode);
        info!("create {:?} {:?}", name, mode);
        if let Some(_dir) = self.i_nodes.get(parent as usize) {
        } else {
            reply.error(ENOENT);
            return;
        };
        self.ensure_active(parent);
        let mut new_file = ActiveFile::new(&self.root, self.root.clone());
        new_file.attra.perm = mode as u16;
        let new_inode = ActiveInode(self.i_nodes.len() as u64);
        new_file.attra.ino = new_inode.0;
        let attra = new_file.attra;
        self.i_nodes.push(INode::ActiveFile(new_file));

        if let Some(parent_dir) = self.i_nodes.get_mut(parent as usize) {
            match parent_dir {
                INode::FileBlob(_) => {
                    panic!("lookup on file inode");
                }
                INode::ActiveFile(_) => todo!(),
                INode::DirBlob(_) => {
                    panic!("ensure should have stopped this");
                }
                INode::ActiveDirectory(parent_dir) => {
                    for file in &parent_dir.files {
                        if file.file_name.as_str() == name {
                            // TODO handdle this proper
                            info!("Creating a file that already exists {file:?}");
                        }
                    }

                    parent_dir.files.push(ActiveDirectoryFile {
                        inode: new_inode.clone(),
                        file_name: name.to_string_lossy().to_string(),
                        _root: self.root.to_owned(),
                    })
                }
                INode::Link(_link) => {
                    panic!("lookup on file inode");
                }
            };
        } else {
            panic!("oh dear me")
        }

        //println!("Createing {:#x?}", new_inode.0);
        info!("Createing {:#x?}", new_inode.0);
        reply.created(&TTL, &attra, 0, 0, flags as u32)
    }

    /// Create a directory.
    fn mkdir(
        &mut self,
        _req: &Request<'_>,
        parent: u64,
        name: &OsStr,
        _mode: u32,
        _umask: u32,
        reply: ReplyEntry,
    ) {
        info!("mkdir {parent:?} {name:?}");
        if let Some(_dir) = self.i_nodes.get(parent as usize) {
        } else {
            reply.error(ENOENT);
            return;
        };
        self.ensure_active(parent);

        self.i_nodes
            .push(INode::ActiveDirectory(ActiveDirectory::new(
                self.root.to_owned(),
                parent,
            )));
        let new_inode = ActiveInode(self.i_nodes.len() as u64 - 1);

        if let Some(parent_dir) = self.i_nodes.get_mut(parent as usize) {
            match parent_dir {
                INode::FileBlob(_) => {
                    panic!("lookup on file inode");
                }
                INode::ActiveFile(_) => todo!(),
                INode::DirBlob(_) => {
                    panic!("ensure should have stopped this");
                }
                INode::ActiveDirectory(parent_dir) => parent_dir
                    .directories
                    .push((new_inode.0, name.to_string_lossy().to_string())),
                INode::Link(_link) => {
                    panic!("lookup on file inode");
                }
            };
        } else {
            panic!("oh dear me")
        }
        let mut attr = HELLO_DIR_ATTR;
        attr.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        attr.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        attr.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
        attr.uid = Uid::current().as_raw();
        attr.gid = Gid::current().as_raw();
        attr.ino = new_inode.0;
        attr.size = 0;

        reply.entry(&TTL, &attr, 0)
    }

    /// Remove a directory.
    fn rmdir(&mut self, _req: &Request<'_>, parent: u64, name: &OsStr, reply: ReplyEmpty) {
        info!("rmdir: {parent:?} {name:?}");
        if let Some(_dir) = self.i_nodes.get(parent as usize) {
        } else {
            reply.error(ENOENT);
            return;
        };
        self.ensure_active(parent);

        let parent_inode = self.i_nodes.get_mut(parent as usize).unwrap();
        let parent_directory = if let INode::ActiveDirectory(directory) = parent_inode {
            directory
        } else {
            panic!("bad tho!")
        };

        let mut found = None;

        for (index, dir) in parent_directory.directories.iter().enumerate() {
            if dir.1 == name.to_str().unwrap() {
                found = Some(index);
            }
        }

        if let Some(dir_index) = found {
            parent_directory.directories.remove(dir_index);
            reply.ok();
        } else {
            panic!("very bad")
        };
    }

    /// Set file attributes.
    fn setattr(
        &mut self,
        _req: &Request<'_>,
        ino: u64,
        mode: Option<u32>,
        uid: Option<u32>,
        gid: Option<u32>,
        size: Option<u64>,
        atime: Option<TimeOrNow>,
        mtime: Option<TimeOrNow>,
        ctime: Option<SystemTime>,
        fh: Option<u64>,
        _crtime: Option<SystemTime>,
        _chgtime: Option<SystemTime>,
        _bkuptime: Option<SystemTime>,
        flags: Option<u32>,
        reply: ReplyAttr,
    ) {
        println!(
            "setattr(ino: {:#x?}, mode: {:?}, uid: {:?}, \
            gid: {:?}, size: {:?}, fh: {:?}, flags: {:?})",
            ino, mode, uid, gid, size, fh, flags
        );
        self.ensure_active(ino);

        if let Some(active_node) = self.i_nodes.get_mut(ino as usize) {
            let mut attr = match active_node {
                INode::FileBlob(_) => {
                    panic!("lookup on file inode");
                }
                INode::ActiveFile(file) => {
                    if let Some(mode) = mode {
                        info!("mode {:#o}", mode);
                        file.attra.perm = mode as u16;
                        info!("pera {:?} {mode:?}", file.attra.perm)
                    }
                    let attr = if let Some(size) = size {
                        let file_handle = std::fs::OpenOptions::new()
                            .write(true)
                            .open(file.file_name())
                            .unwrap();
                        file_handle.set_len(size).unwrap();

                        file.attra.size = size;
                        file.attra.blocks = file.attra.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
                        file.attra
                    } else {
                        file.attra.size = file.file_name().metadata().unwrap().len();
                        file.attra.blocks = file.attra.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
                        file.attra
                    };
                    attr
                }
                INode::DirBlob(_) => {
                    panic!("ensure should have stopped this");
                }
                INode::ActiveDirectory(_active_dir) => {
                    info!("ActiveDir setattr");
                    let mut attr = HELLO_DIR_ATTR;
                    attr.mtime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.ctime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.atime = UNIX_EPOCH + Duration::from_secs(40 * 360 * 24 * 3600);
                    attr.uid = Uid::current().as_raw();
                    attr.gid = Gid::current().as_raw();
                    attr.ino = ino;
                    attr
                }
                INode::Link(link) => {
                    let attr = link.attra;
                    assert_eq!(attr.ino, ino);
                    attr
                }
            };
            if let Some(ctime) = ctime {
                info!("set ctime");
                attr.ctime = ctime;
            }
            if let Some(atime) = atime {
                info!("set atime");
                attr.atime = match atime {
                    TimeOrNow::SpecificTime(time) => time,
                    fuser::TimeOrNow::Now => SystemTime::now(),
                };
            } else {
                attr.atime = SystemTime::now();
            }
            if let Some(mtime) = mtime {
                info!("set mtime");
                attr.mtime = match mtime {
                    TimeOrNow::SpecificTime(time) => time,
                    fuser::TimeOrNow::Now => SystemTime::now(),
                };
            } else {
                attr.atime = SystemTime::now();
            }
            if let Some(flags) = flags {
                info!("flags: {flags:?}");
                attr.flags = flags;
            }
            if let Some(mode) = mode {
                attr.perm = mode as u16;
                info!("mode {mode:?} {:?}", attr.perm);
            }
            if let Some(uid) = uid {
                attr.uid = uid;
                info!("uid {uid:?}");
                attr.ctime = SystemTime::now();
                attr.mtime = SystemTime::now();
                attr.atime = SystemTime::now();
            }
            if let Some(gid) = gid {
                attr.gid = gid;
                info!("mode {gid:?}");
            }

            info!("attr {attr:?}");

            reply.attr(&TTL, &attr);
            return;
        }
        reply.error(ENOSYS);
    }

    fn opendir(&mut self, _req: &Request, _inode: u64, _flags: i32, reply: ReplyOpen) {
        //println!("opendir {inode:?}");
        reply.opened(0, 0);
    }

    /// Remove a file.
    fn unlink(&mut self, _req: &Request<'_>, parent: u64, name: &OsStr, reply: ReplyEmpty) {
        self.ensure_active(parent);
        let active_parent = self.i_nodes.get_mut(parent as usize).unwrap();
        let active_dir = if let INode::ActiveDirectory(active_dir) = active_parent {
            active_dir
        } else {
            panic!("bad")
        };

        let mut found_file = None;

        for (index, file) in active_dir.files.iter().enumerate() {
            if file.file_name == name.to_str().unwrap() {
                found_file = Some(index);
            }
        }

        if let Some(file_index) = found_file {
            let removed = active_dir.files.remove(file_index);
            let file = self.i_nodes.get_mut(removed.inode.0 as usize).unwrap();
            if let INode::ActiveFile(afile) = file {
                afile.attra.nlink -= 1;
            } else if let INode::FileBlob(bfile) = file {
                bfile.attr.nlink -= 1;
            } else {
                reply.error(ENOSYS);
                return;
            }
            reply.ok()
        } else {
            let mut found_dir = None;

            for (index, dir) in active_dir.directories.iter().enumerate() {
                if dir.1 == name.to_str().unwrap() {
                    found_dir = Some(index);
                }
            }

            if let Some(dir_index) = found_dir {
                active_dir.directories.remove(dir_index);
                reply.ok()
            } else {
                let mut found_link = None;

                for (index, link) in active_dir.links.iter().enumerate() {
                    if link.1 == name.to_str().unwrap() {
                        found_link = Some(index);
                    }
                }

                if let Some(link_index) = found_link {
                    active_dir.links.remove(link_index);
                    reply.ok()
                } else {
                    reply.error(ENOSYS)
                }
            }
        }
    }

    /// Check file access permissions.
    /// This will be called for the access() system call. If the 'default_permissions'
    /// mount option is given, this method is not called. This method is not called
    /// under Linux kernel versions 2.4.x
    fn access(&mut self, _req: &Request<'_>, ino: u64, mask: i32, reply: ReplyEmpty) {
        // you should check and reply if mask machets

        info!("access {ino:?} {mask:?}");

        let inode = if let Some(inode) = self.i_nodes.get(ino as usize) {
            inode
        } else {
            reply.error(ENOENT);
            return;
        };

        match inode {
            INode::FileBlob(cas_file) => {
                if (cas_file.attr.perm & mask as u16) == mask as u16 {
                    reply.ok();
                } else {
                    reply.error(EACCES)
                }
            }
            INode::ActiveFile(file) => {
                if (file.attra.perm & mask as u16) == mask as u16 {
                    reply.ok();
                } else {
                    reply.error(EACCES)
                }
            }
            INode::DirBlob(_) => reply.ok(),
            INode::ActiveDirectory(_) => {
                // TODO use extended attra to make disistion
                reply.ok()
            }
            INode::Link(link) => {
                if (link.attra.perm & mask as u16) == mask as u16 {
                    reply.ok();
                } else {
                    reply.error(EACCES)
                }
            }
        }
    }

    fn readdir(
        &mut self,
        _req: &Request,
        ino: u64,
        _fh: u64,
        offset: i64,
        mut reply: ReplyDirectory,
    ) {
        info!("readdir offset {offset}");
        //println!("inode {}", ino);

        assert!(offset >= 0);

        self.ensure_active(ino);

        if let Some(inode) = self.i_nodes.get_mut(ino as usize) {
            match inode {
                INode::FileBlob(_) | INode::DirBlob(_) => {
                    reply.error(ENOENT);
                    return;
                }
                INode::ActiveDirectory(active) => {
                    let full_list = newbit(ino, active);
                    //println!("len {} {:?}", full_list.len(), full_list);
                    for (ino, offset, file_type, name) in
                        full_list.into_iter().skip(offset as usize)
                    {
                        let buffer_full: bool =
                            reply.add(ino, offset, file_type, OsStr::from_bytes(name.as_bytes()));

                        if buffer_full {
                            info!("finish early {offset}");
                            break;
                        }
                    }
                    //println!("blar");
                }
                INode::Link(_link) => {
                    panic!("I didnt think this would happen")
                }
                INode::ActiveFile(_) => {
                    panic!("I didnt think this would happen")
                }
            }
        } else {
            println!("bad news");
            reply.error(ENOENT);
            return;
        };
        //println!("reply: {reply:?}");
        //panic!("stop");

        reply.ok();
    }

    fn readdirplus(
        &mut self,
        _req: &Request<'_>,
        ino: u64,
        fh: u64,
        offset: i64,
        reply: ReplyDirectoryPlus,
    ) {
        info!(
            "[Not Implemented] readdirplus(ino: {:#x?}, fh: {}, offset: {})",
            ino, fh, offset
        );
        reply.error(ENOSYS);
    }

    fn flush(&mut self, _req: &Request, _ino: u64, _fh: u64, _lock_owner: u64, reply: ReplyEmpty) {
        info!("flush");
        reply.ok();
    }

    fn release(
        &mut self,
        _req: &Request<'_>,
        _ino: u64,
        _fh: u64,
        _flags: i32,
        _lock_owner: Option<u64>,
        _flush: bool,
        reply: ReplyEmpty,
    ) {
        reply.ok();
    }

    /// Read symbolic link.
    fn readlink(&mut self, _req: &Request<'_>, inode: u64, reply: ReplyData) {
        info!("Do a link read {inode}");

        if let Some(file_inode) = self.i_nodes.get(inode as usize) {
            //println!("{file_inode:?}");
            match file_inode {
                INode::FileBlob(_) => {
                    println!("reading a file not a link");
                    reply.error(ENOENT);
                    return;
                }
                INode::DirBlob(_) => {
                    println!("Cant read a directory");
                    reply.error(ENOENT);
                    return;
                }
                INode::ActiveDirectory(_) => {
                    println!("Cant read a directory");
                    reply.error(ENOENT);
                    return;
                }
                INode::Link(link) => {
                    //println!("read a link: {name}");
                    //
                    assert_eq!(inode, link.attra.ino);
                    reply.data(link.target.as_bytes());
                    return;
                }
                INode::ActiveFile(_) => todo!(),
            }
        }

        reply.error(ENOENT);
    }

    /// Rename a file.
    fn rename(
        &mut self,
        _req: &Request<'_>,
        parent: u64,
        name: &OsStr,
        newparent: u64,
        newname: &OsStr,
        _flags: u32,
        reply: ReplyEmpty,
    ) {
        info!("rename from {parent:#x} {name:?} to {newparent:#x} {newname:?}");
        self.ensure_active(parent);
        self.ensure_active(newparent);

        let old_parent_inode = self.i_nodes.get_mut(parent as usize).unwrap();

        let found = if let INode::ActiveDirectory(old_parent) = old_parent_inode {
            let mut found = None;

            if parent == newparent {
                let mut old = None;
                for (index, file) in old_parent.files.iter().enumerate() {
                    if file.file_name == newname.to_str().unwrap() {
                        old = Some(index);
                    }
                }
                if let Some(index) = old {
                    old_parent.files.remove(index);
                }
            };
            for (index, file) in old_parent.files.iter_mut().enumerate() {
                if file.file_name == name.to_str().unwrap() {
                    file.file_name = newname.to_string_lossy().to_string();
                    info!("Renamed file {:x} {:?}", file.inode.0, file);
                    if parent == newparent {
                        reply.ok();
                        return;
                    };
                    found = Some(ActiveDirectoryNode::File(
                        index,
                        old_parent.files.remove(index),
                    ));
                    break;
                }
            }

            if parent == newparent {
                let mut old = None;
                for (index, dir) in old_parent.directories.iter().enumerate() {
                    if dir.1 == newname.to_str().unwrap() {
                        old = Some(index);
                    }
                }
                if let Some(index) = old {
                    old_parent.directories.remove(index);
                }
            };
            if found.is_none() {
                for (index, dir) in old_parent.directories.iter_mut().enumerate() {
                    if dir.1 == name.to_str().unwrap() {
                        dir.1 = newname.to_string_lossy().to_string();
                        if parent == newparent {
                            reply.ok();
                            return;
                        };
                        found = Some(ActiveDirectoryNode::Dir(
                            index,
                            old_parent.directories.remove(index),
                        ));
                        break;
                    }
                }
            }

            if parent == newparent {
                let mut old = None;
                for (index, lnk) in old_parent.links.iter().enumerate() {
                    if lnk.1 == newname.to_str().unwrap() {
                        old = Some(index);
                    }
                }
                if let Some(index) = old {
                    old_parent.links.remove(index);
                }
            };
            if found.is_none() {
                for (index, link) in old_parent.links.iter_mut().enumerate() {
                    if link.1 == name.to_str().unwrap() {
                        link.1 = newname.to_string_lossy().to_string();
                        if parent == newparent {
                            reply.ok();
                            return;
                        };
                        found = Some(ActiveDirectoryNode::Link(
                            index,
                            old_parent.links.remove(index),
                        ));
                        break;
                    }
                }
            };
            found
        } else {
            panic!("bad")
        };
        if let Some(INode::ActiveDirectory(new_parent)) = self.i_nodes.get_mut(newparent as usize) {
            match found.unwrap() {
                ActiveDirectoryNode::File(_index, file) => {
                    let mut old = None;
                    for (index, file) in new_parent.files.iter().enumerate() {
                        if file.file_name == newname.to_str().unwrap() {
                            old = Some(index);
                        }
                    }
                    if let Some(index) = old {
                        new_parent.files.remove(index);
                    }
                    new_parent.files.push(file);
                }
                ActiveDirectoryNode::Dir(_index, directrory) => {
                    let mut old = None;
                    for (index, dir) in new_parent.directories.iter().enumerate() {
                        if dir.1 == newname.to_str().unwrap() {
                            old = Some(index);
                        }
                    }
                    if let Some(index) = old {
                        new_parent.directories.remove(index);
                    }
                    new_parent.directories.push(directrory);
                }
                ActiveDirectoryNode::Link(_index, link) => {
                    let mut old = None;
                    for (index, lnk) in new_parent.links.iter().enumerate() {
                        if lnk.1 == newname.to_str().unwrap() {
                            old = Some(index);
                        }
                    }
                    if let Some(index) = old {
                        new_parent.links.remove(index);
                    }
                    new_parent.links.push(link);
                }
            }
            reply.ok();
            return;
        }
        reply.error(ENOSYS);
    }

    fn read(
        &mut self,
        _req: &Request,
        inode: u64,
        _fh: u64,
        offset: i64,
        size: u32,
        flags: i32,
        _lock_owner: Option<u64>,
        reply: ReplyData,
    ) {
        info!("read(ino: {inode:#x} offset: {offset} size: {size})");
        assert!(offset >= 0);

        if let Some(file_inode) = self.i_nodes.get_mut(inode as usize) {
            //println!("{file_inode:?}");
            match file_inode {
                INode::FileBlob(cas_file) => {
                    let new_path = digest_to_path(&self.root, &cas_file.blob);

                    let mut open_file = std::fs::File::open(new_path).unwrap();
                    if offset > 0 {
                        open_file
                            .seek(std::io::SeekFrom::Start(offset as u64))
                            .unwrap();
                    };

                    let mut total_buf = vec![
                        0;
                        min(
                            size as usize,
                            cas_file.blob.size_bytes as usize - offset as usize
                        ) as usize
                    ];
                    open_file.flush().unwrap();
                    open_file.read_exact(&mut total_buf).unwrap();
                    if (flags & O_NOATIME) != O_NOATIME {
                        cas_file.attr.atime = SystemTime::now();
                    }
                    reply.data(&total_buf);
                    return;
                }
                INode::DirBlob(_) => {
                    println!("Cant read a directory");
                    reply.error(ENOENT);
                    return;
                }
                INode::ActiveDirectory(_) => {
                    println!("Cant read a directory");
                    reply.error(ENOENT);
                    return;
                }
                INode::Link(link) => {
                    //println!("read a link: {name}");
                    assert_eq!(link.attra.ino, inode);
                    reply.data(link.target.as_bytes());
                    return;
                }
                INode::ActiveFile(file) => {
                    let mut file_handle = std::fs::File::open(file.file_name()).unwrap();
                    file_handle.sync_all().unwrap();
                    let file_size = file_handle.metadata().unwrap().len();
                    //println!("update time {:?}", SystemTime::now());
                    if (flags & O_NOATIME) != O_NOATIME {
                        file.attra.atime = SystemTime::now();
                    }
                    if (offset as u64) <= file_size {
                        let mut total_buf = vec![
                            0;
                            min(size as usize, file_size as usize - offset as usize)
                                as usize
                        ];
                        file_handle.flush().unwrap();
                        if offset > 0 {
                            file_handle.seek(SeekFrom::Start(offset as u64)).unwrap();
                        }
                        file_handle.read_exact(&mut total_buf).unwrap();
                        reply.data(&total_buf);
                    } else {
                        reply.data(&[]);
                    }
                    return;
                }
            }
        }

        reply.error(ENOENT);
    }

    fn write(
        &mut self,
        _req: &Request<'_>,
        ino: u64,
        fh: u64,
        offset: i64,
        data: &[u8],
        write_flags: u32,
        flags: i32,
        lock_owner: Option<u64>,
        reply: ReplyWrite,
    ) {
        info!(
            "write(ino: {:#x?}, fh: {}, offset: {}, data.len(): {}, \
            write_flags: {:#x?}, flags: {:#x?}, lock_owner: {:?})",
            ino,
            fh,
            offset,
            data.len(),
            write_flags,
            flags,
            lock_owner
        );

        self.ensure_active(ino);
        //if write_flags != 0 {
        //    panic!("flags write {:?}", write_flags)
        //}
        //if flags != 0 {
        //    panic!("flags {:?}", flags)
        //}

        if let Some(file_inode) = self.i_nodes.get_mut(ino as usize) {
            //println!("{file_inode:?}");
            match file_inode {
                INode::FileBlob(_) => {
                    println!("Ensure means we should never get here");
                    reply.error(ENOENT);
                    return;
                }
                INode::DirBlob(_) => {
                    println!("Cant write a directory");
                    reply.error(ENOENT);
                    return;
                }
                INode::ActiveDirectory(_) => {
                    println!("Cant write a directory");
                    reply.error(ENOENT);
                    return;
                }
                INode::Link(link) => {
                    link.target = String::from_utf8(Vec::from(data)).unwrap();
                    link.attra.size = data.len() as u64;
                    link.attra.blocks = link.attra.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
                    link.attra.mtime = SystemTime::now();
                    link.attra.atime = SystemTime::now();
                    link.attra.ctime = SystemTime::now();
                    reply.written(data.len() as u32);
                    return;
                }
                INode::ActiveFile(file) => {
                    assert!(offset >= 0);
                    file.attra.mtime = SystemTime::now();
                    file.attra.atime = SystemTime::now();
                    file.attra.ctime = SystemTime::now();
                    if !file.file_name().exists() {
                        let mut file_handle = std::fs::File::create(file.file_name()).unwrap();
                        if offset > 0 {
                            file_handle.seek(SeekFrom::Start(offset as u64)).unwrap();
                        }
                        file_handle.write_all(data).unwrap();
                        file_handle.flush().unwrap();
                        file.attra.size = file.file_name().metadata().unwrap().len();
                        file.attra.blocks = file.attra.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
                        reply.written(data.len() as u32);
                    } else {
                        let mut file_handle = std::fs::OpenOptions::new()
                            .write(true)
                            .open(file.file_name())
                            .unwrap();
                        file_handle.seek(SeekFrom::Start(offset as u64)).unwrap();
                        file_handle.write_all(data).unwrap();
                        file_handle.flush().unwrap();
                        file.attra.size = file.file_name().metadata().unwrap().len();
                        file.attra.blocks = file.attra.size.div_ceil(LOGICAL_BLOCKSIZE as u64);
                        reply.written(data.len() as u32);
                    }
                    return;
                }
            }
        }

        reply.error(ENOSYS);
    }

    /// Create a symbolic link.
    fn symlink(
        &mut self,
        _req: &Request<'_>,
        parent: u64,
        link_name: &OsStr,
        target: &Path,
        reply: ReplyEntry,
    ) {
        info!("symlink");
        self.ensure_active(parent);

        let link_target = target.to_string_lossy().to_string();
        let mut link = ActiveLink::new(parent, link_target);
        let link_name = link_name.to_str().unwrap().to_string();
        let new_ino = self.i_nodes.len() as u64;
        link.attra.ino = new_ino;
        let attr = link.attra;
        self.i_nodes.push(INode::Link(link));
        if let Some(file_inode) = self.i_nodes.get_mut(parent as usize) {
            //println!("{file_inode:?}");
            match file_inode {
                INode::FileBlob(_) => todo!(),
                INode::ActiveFile(_) => todo!(),
                INode::DirBlob(_) => todo!(),
                INode::ActiveDirectory(active_dir) => {
                    active_dir.links.push((new_ino, link_name));
                    reply.entry(&TTL, &attr, 0);
                    return;
                }
                INode::Link(_) => todo!(),
            };
        }
        reply.error(ENOENT);
    }

    fn destroy(&mut self) {
        let dummy = INode::Link(ActiveLink::new(0, "".to_string()));
        self.i_nodes.push(dummy);

        let root = self.i_nodes.swap_remove(1);

        if let INode::DirBlob((_, blob)) = root {
            self.reply.send(blob.to_owned()).unwrap();
        } else if let INode::ActiveDirectory(active_dir) = root {
            self.reply
                .send(active_dir.get_digest(&mut self.i_nodes))
                .unwrap();
            return;
        } else {
            panic!("Root node must be directory")
        }
    }

    /// Create a hard link.
    fn link(
        &mut self,
        _req: &Request<'_>,
        ino: u64,
        newparent: u64,
        newname: &OsStr,
        reply: ReplyEntry,
    ) {
        println!("do a link {ino} {newparent} {newname:?} ");
        self.ensure_active(ino);
        self.ensure_active(newparent);

        let target = self.i_nodes.get_mut(ino as usize).unwrap();

        let attr = if let INode::ActiveFile(file) = target {
            file.attra.nlink += 1;
            // Should the times be updated ?
            // file.attra.mtime = SystemTime::now();
            // file.attra.atime = SystemTime::now();
            file.attra
        } else {
            debug!("Nope");
            reply.error(EIO);
            return;
        };
        let new_parent = self.i_nodes.get_mut(newparent as usize).unwrap();
        if let INode::ActiveDirectory(newparent) = new_parent {
            newparent.files.push(ActiveDirectoryFile {
                inode: ActiveInode(ino),
                file_name: newname.to_str().unwrap().to_owned(),
                _root: self.root.clone(),
            })
        } else {
            debug!("Nope");
            reply.error(EIO);
            return;
        }
        reply.entry(&TTL, &attr, 0);
    }
}

#[cfg(test)]
mod tests {
    use std::os::unix::fs::{MetadataExt, OpenOptionsExt, PermissionsExt};
    use std::time::{self, Duration, SystemTime, UNIX_EPOCH};
    use std::{
        fs::{self, create_dir_all, File},
        io::{Read, Seek, SeekFrom, Write},
        os::linux::fs::MetadataExt as _,
        process::Command,
        sync::mpsc::channel,
    };
    use std::{thread, usize};

    use crate::directory_from_digest;
    use crate::{
        digest_from_directory, directory_from_digest_block, fuse::ReapiFilesystem,
        upload_blob_block,
    };
    use fuser::MountOption;
    use nix::NixPath;
    use reapi::build::bazel::remote::execution::v2::{
        command::EnvironmentVariable, Digest, Directory, FileNode,
    };
    use rstest::rstest;
    use tempfile::{tempdir, TempDir};

    #[test]
    fn basic_fs() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let content = "Hello World".to_string();
        let file_di = upload_blob_block(&cas, content.as_bytes().to_vec());

        let (sender, receiver) = channel();
        let root_dir = Directory {
            files: vec![FileNode {
                name: "bob".to_string(),
                digest: Some(file_di),
                is_executable: false,
                node_properties: None,
            }],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");

        let options = vec![MountOption::RW, MountOption::FSName("basic_fs".to_string())];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let mut read_file = File::open(mount.join("bob")).unwrap();
        let mut read_content = vec![];
        read_file.read_to_end(&mut read_content).unwrap();
        assert_eq!(String::from_utf8(read_content).unwrap(), content);
        drop(read_file);

        let mut test_file = File::create(mount.join("test_file")).unwrap();
        test_file.write_all("Hi there".as_bytes()).unwrap();
        drop(test_file);

        drop(filesystem);
        let root_digest = receiver.recv().unwrap();
        println!("reply: {root_digest:?}");

        let returned_dir = directory_from_digest_block(&cas, root_digest);

        assert_eq!(returned_dir.files.len(), 2);
        assert_eq!(returned_dir.directories.len(), 0);
    }

    #[test]
    fn write_append() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello there\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = std::fs::OpenOptions::new()
            .append(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello again\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = fs::File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello there\nHello again\n".as_bytes(), &buf);
        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn write_rewrite() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello there\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = std::fs::OpenOptions::new()
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle
            .write_all("Hello again and again\n".as_bytes())
            .unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = fs::File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello again and again\n".as_bytes(), &buf);
        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn write_seek_truncate() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello there\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        //.seek(std::io::SeekFrom::Start(offset as u64))
        file_handle.seek(SeekFrom::Start(0)).unwrap();
        file_handle
            .write_all("Hello again and again\n".as_bytes())
            .unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = fs::File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello again and again\n".as_bytes(), &buf);
        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn write_seek_start() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello there\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        //.seek(std::io::SeekFrom::Start(offset as u64))
        file_handle.seek(SeekFrom::Start(6)).unwrap();
        file_handle
            .write_all("Hello again and again\n".as_bytes())
            .unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = fs::File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello Hello again and again\n".as_bytes(), &buf);
        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn write_seek_end() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello there\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        file_handle.seek(SeekFrom::End(-6)).unwrap();
        file_handle
            .write_all("Hello again and again\n".as_bytes())
            .unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = fs::File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello Hello again and again\n".as_bytes(), &buf);
        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn delete() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello there\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        fs::remove_file(&read_write_file).unwrap();

        drop(filesystem);
        let final_state = receiver.recv().unwrap();

        let final_directory = directory_from_digest_block(&cas, final_state);
        assert_eq!(final_directory.files.len(), 0);
        assert_eq!(final_directory.directories.len(), 0);
        assert_eq!(final_directory.symlinks.len(), 0);
    }

    #[test]
    fn write_big() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        let mut big_boi = String::new();

        for iii in 0..100000 {
            big_boi.push_str(&format!("Another great line {iii}\n"));
        }
        file_handle.write_all(big_boi.as_bytes()).unwrap();

        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = fs::File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        assert_eq!(big_boi, String::from_utf8(buf).unwrap());

        file_handle.seek(SeekFrom::Start(10000)).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        assert_eq!(big_boi.as_bytes()[10000..], buf);

        drop(file_handle);

        drop(filesystem);
        let final_state = receiver.recv().unwrap();

        let final_directory = directory_from_digest_block(&cas, final_state);
        assert_eq!(final_directory.files.len(), 1);

        assert_eq!(
            final_directory
                .files
                .first()
                .as_ref()
                .unwrap()
                .digest
                .as_ref()
                .unwrap()
                .size_bytes,
            big_boi.as_bytes().len() as i64
        );
    }

    #[test]
    fn write_trucate() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle
            .write_all("Hello there, what a lot of text\n".as_bytes())
            .unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = std::fs::OpenOptions::new()
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let mut file_handle = fs::File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello\n".as_bytes(), &buf);
        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn hard_link() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);
        let meta = std::fs::metadata(&read_write_file).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());

        let target = mount.join("targ");
        std::fs::hard_link(&read_write_file, &target).unwrap();

        let mut file_handle = fs::File::open(&target).unwrap();
        let meta = std::fs::metadata(&target).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello\n".as_bytes(), &buf);
        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn sym_link() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        let content = "Hello everyone\ntwo line\n".as_bytes();
        file_handle.write_all(content).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);
        let meta = std::fs::metadata(&read_write_file).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());

        let link = mount.join("link");
        std::os::unix::fs::symlink(&read_write_file, &link).unwrap();

        let mut file_handle = fs::File::open(&link).unwrap();
        let meta = std::fs::metadata(&link).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        assert_eq!(buf, content);
        drop(file_handle);

        let meta = std::fs::symlink_metadata(&link).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());
        assert!(meta.is_symlink());

        let dir1 = mount.join("dir1");
        create_dir_all(&dir1).unwrap();
        let link2 = dir1.join("link2");

        let link_target = std::path::PathBuf::from("../file1");
        std::os::unix::fs::symlink(&link_target, &link2).unwrap();

        let mut file_handle = fs::File::open(&link2).unwrap();
        let meta = std::fs::metadata(&link2).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        assert_eq!(buf, content);
        drop(file_handle);

        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn rename() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle.write_all("Hello\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);
        let meta = std::fs::metadata(&read_write_file).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());

        let target = mount.join("targ");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&target)
            .unwrap();

        file_handle.write_all("Dummy Data\n".as_bytes()).unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);
        let meta = std::fs::metadata(&read_write_file).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());

        std::fs::rename(&read_write_file, &target).unwrap();

        let mut file_handle = fs::File::open(&target).unwrap();
        let meta = std::fs::metadata(&target).unwrap();
        println!("meta, {:?} {:?} {meta:?}", meta.ino(), meta.size());
        let mut buf = vec![];
        file_handle.read_to_end(&mut buf).unwrap();
        drop(file_handle);
        assert_eq!("Hello\n".as_bytes(), &buf);
        drop(filesystem);
        let final_state = receiver.recv().unwrap();

        let final_directory = directory_from_digest_block(&cas, final_state);
        assert_eq!(final_directory.files.len(), 1);
    }

    #[test]
    fn attr() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::File::create(&read_write_file).unwrap();
        file_handle.write_all(b"some data to write").unwrap();
        drop(file_handle);
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs() as i64;

        let meta = std::fs::metadata(&read_write_file).unwrap();
        println!("now {now:?}, meta {meta:?}");
        let atime = meta.atime();
        assert!((atime - now).abs() <= 1);
        let mtime = meta.mtime();
        assert!((mtime - now).abs() <= 1);
        let ctime = meta.ctime();
        assert!((ctime - now).abs() <= 1);

        let some_sec = time::Duration::from_secs(2);
        thread::sleep(some_sec);
        println!("slept");

        let mut file_handle = File::open(&read_write_file).unwrap();
        let mut buf = vec![];
        println!("read");
        let _ = file_handle.read_to_end(&mut buf).unwrap();
        println!("buf: {buf:?}");
        drop(file_handle);
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs() as i64;

        let meta = std::fs::metadata(&read_write_file).unwrap();
        let atime = meta.atime();
        println!("now {now:?}, meta {meta:?}");
        assert!((atime - now).abs() <= 1);
        let mtime = meta.mtime();
        dbg!(mtime, now);
        assert!((mtime - now).abs() >= 1);
        let ctime = meta.ctime();
        assert!((ctime - now).abs() >= 1);

        let mut file_handle = std::fs::OpenOptions::new()
            .append(true)
            .open(&read_write_file)
            .unwrap();
        let _ = file_handle.write("hi there".as_bytes()).unwrap();
        drop(file_handle);
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs() as i64;

        let meta = std::fs::metadata(read_write_file).unwrap();
        let atime = meta.atime();
        assert!((atime - now).abs() <= 1);
        let mtime = meta.mtime();
        dbg!(mtime, now);
        assert!((mtime - now).abs() <= 1);
        let ctime = meta.ctime();
        assert!((ctime - now).abs() <= 1);

        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[test]
    fn cas_mode() {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let file1 = FileNode {
            name: "cas1".to_string(),
            digest: Some(Digest {
                hash: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
                    .to_string(),
                size_bytes: 0,
            }),
            is_executable: true,
            node_properties: None,
        };
        let file2 = FileNode {
            name: "cas2".to_string(),
            digest: Some(Digest {
                hash: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
                    .to_string(),
                size_bytes: 0,
            }),
            is_executable: false,
            node_properties: None,
        };

        let root_dir = Directory {
            files: vec![file1, file2],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let meta = std::fs::metadata(mount.join("cas1")).unwrap();
        let pem = meta.permissions().mode();
        println!("{pem:#o}");
        assert_eq!(pem, 0o100755);

        let meta = std::fs::metadata(mount.join("cas2")).unwrap();
        let pem = meta.permissions().mode();
        println!("{pem:#o}");
        assert_eq!(pem, 0o100644);

        drop(filesystem);
        receiver.recv().unwrap();
    }

    #[rstest]
    #[case(0o100755)]
    #[case(0o100644)]
    #[case(0o100700)]
    #[case(0o100600)]
    #[test]
    fn create_bin(#[case] mode: u32) {
        let tmp_dir_test = tempdir().unwrap();

        let tmp_root = tmp_dir_test.path();

        let cas = tmp_root.join("cas");
        let mount = tmp_root.join("mount");
        create_dir_all(&mount).unwrap();

        let (sender, receiver) = channel();

        let root_dir = Directory {
            files: vec![],
            directories: vec![],
            symlinks: vec![],
            node_properties: None,
        };

        let rood_di = digest_from_directory(&cas, root_dir);

        let cas_fs = ReapiFilesystem::new(cas.clone(), rood_di, sender);
        println!("cas_fs: {cas_fs:?}");
        let options = vec![
            MountOption::RW,
            MountOption::FSName("hello".to_string()),
            //MountOption::AutoUnmount,
        ];
        let filesystem = fuser::spawn_mount2(cas_fs, &mount, &options).unwrap();

        let read_write_file = mount.join("file1");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .mode(mode)
            .open(&read_write_file)
            .unwrap();

        file_handle
            .write_all("#!/bin/bash\necho Execute\n".as_bytes())
            .unwrap();
        file_handle.flush().unwrap();
        drop(file_handle);

        let meta = std::fs::metadata(&read_write_file).unwrap();
        let pem = meta.permissions().mode();
        println!("{pem:#o} {mode:#o}");
        assert_eq!(pem, mode);

        let read_write_file = mount.join("file2");
        let mut file_handle = std::fs::OpenOptions::new()
            .create(true)
            .truncate(true)
            .write(true)
            .open(&read_write_file)
            .unwrap();

        file_handle
            .write_all("#!/bin/bash\necho Execute\n".as_bytes())
            .unwrap();
        file_handle.flush().unwrap();
        fs::set_permissions(&read_write_file, fs::Permissions::from_mode(mode)).unwrap();

        drop(file_handle);

        let meta = std::fs::metadata(&read_write_file).unwrap();
        let pem = meta.permissions().mode();
        println!("{pem:#o} {mode:#o}");
        assert_eq!(pem, mode);

        drop(filesystem);
        let final_state = receiver.recv().unwrap();

        let final_directory = directory_from_digest_block(&cas, final_state);
        assert_eq!(final_directory.files.len(), 2);
        assert_eq!(
            final_directory.files.first().unwrap().is_executable,
            0o100 & mode == 0o100
        );
        assert_eq!(
            final_directory.files.get(1).unwrap().is_executable,
            0o100 & mode == 0o100
        );
    }
}
